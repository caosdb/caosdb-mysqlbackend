/*
 * ** header v3.0
 * This file is a part of the CaosDB Project.
 *
 * Copyright (C) 2018 Research Group Biomedical Physics,
 * Max-Planck-Institute for Dynamics and Self-Organization Göttingen
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 *
 * ** end header
 */


DROP PROCEDURE IF EXISTS db_5_0.entityACL;
delimiter //
CREATE PROCEDURE db_5_0.entityACL(out ACLID INT UNSIGNED, in ACLSTR VARBINARY(65525))
BEGIN
   SELECT id INTO ACLID FROM entity_acl as t WHERE t.acl=ACLSTR LIMIT 1;
   IF ACLID IS NULL THEN
        INSERT INTO entity_acl (acl) VALUES (ACLSTR);
        SET ACLID = LAST_INSERT_ID();
   END IF;
END;
//
delimiter ;

/*
 * This file is a part of the CaosDB Project.
 *
 * Copyright (C) 2018 Research Group Biomedical Physics,
 * Max-Planck-Institute for Dynamics and Self-Organization Göttingen
 * Copyright (C) 2020,2023 Indiscale GmbH <info@indiscale.com>
 * Copyright (C) 2020,2023 Timm Fitschen <t.fitschen@indiscale.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */

DROP PROCEDURE IF EXISTS db_5_0.retrieveOverrides;
delimiter //

/*
 * Retrieve the overridden (overriding the Abstract Property's) name, description, and datatype.
 *
 * Parameters
 * ----------
 *
 * DomainID : VARCHAR(255)
 *     The domain id (0 or the entity'id for level-2-data)
 * EntityID : VARCHAR(255)
 *     The entity id (or the property'id for level-2-data)
 * Version : VARBINARY(255)
 *     The version of the entity. Optional
 *
 * ResultSet
 * ---------
 * collection_override, # e.g. LIST
 * name_override,       # the name
 * desc_override,       # the description
 * type_name_override,  # the datatype, e.g. DOUBLE
 * type_id_override,    # data type id.
 * entity_id,           # same as input EntityID
 * InternalPropertyID,  # internal property id, to be used when property_id
 *                      # is NULL because a replacement is used.
 * property_id,         # the property id
 */
CREATE PROCEDURE db_5_0.retrieveOverrides(
    in DomainID VARCHAR(255),
    in EntityID VARCHAR(255),
    in Version VARBINARY(255))
retrieveOverridesBody: BEGIN

    DECLARE IVersion INT UNSIGNED DEFAULT NULL;
    DECLARE IsHead BOOLEAN DEFAULT TRUE;
    DECLARE InternalEntityID INT UNSIGNED DEFAULT NULL;
    DECLARE InternalDomainID INT UNSIGNED DEFAULT 0;

    -- When DomainID != 0 the EntityID could possibly be a 'replacement id'
    -- which are internal ids by definition (and do not have external
    -- equivalents).
    IF LOCATE("$", EntityID) = 1 THEN
        SET InternalEntityID=SUBSTRING(EntityID, 2);
    ELSE
        SELECT internal_id INTO InternalEntityID FROM entity_ids WHERE id = EntityID;
    END IF;
    -- DomainID != 0 are always normal (i.e. external) Entity ids.
    SELECT internal_id INTO InternalDomainID from entity_ids WHERE id = DomainID;

    IF is_feature_config("ENTITY_VERSIONING", "ENABLED") THEN
        IF Version IS NOT NULL THEN
            IF InternalDomainID = 0 THEN
                SELECT get_head_version(EntityID) = Version INTO IsHead;
            ELSE
                SELECT get_head_version(DomainID) = Version INTO IsHead;
            END IF;
        END IF;

        IF IsHead IS FALSE THEN
            SELECT e._iversion INTO IVersion
                FROM entity_version as e
                WHERE ((e.entity_id = InternalEntityID AND InternalDomainID = 0)
                    OR (e.entity_id = InternalDomainID))
                AND e.version = Version;

            IF IVersion IS NULL THEN
                -- RETURN EARLY - Version does not exist.
                LEAVE retrieveOverridesBody;
            END IF;

            -- name
            SELECT
                NULL AS collection_override,
                name AS name_override,
                NULL AS desc_override,
                NULL AS type_name_override,
                NULL AS type_id_override,
                EntityID AS entity_id,
                CONCAT("$", property_id) AS InternalPropertyID,
                ( SELECT id FROM entity_ids WHERE internal_id = property_id ) AS property_id
            FROM archive_name_overrides
            WHERE domain_id = InternalDomainID
            AND entity_id = InternalEntityID
            AND _iversion = IVersion

            UNION ALL

            -- description
            SELECT
                NULL AS collection_override,
                NULL AS name_override,
                description AS desc_override,
                NULL AS type_name_override,
                NULL AS type_id_override,
                EntityID AS entity_id,
                CONCAT("$", property_id) AS InternalPropertyID,
                ( SELECT id FROM entity_ids WHERE internal_id = property_id ) AS property_id
            FROM archive_desc_overrides
            WHERE domain_id = InternalDomainID
            AND entity_id = InternalEntityID
            AND _iversion = IVersion

            UNION ALL

            -- data type
            SELECT
                NULL AS collection_override,
                NULL AS name_override,
                NULL AS desc_override,
                (SELECT value FROM name_data
                    WHERE domain_id = 0
                    AND entity_id = datatype
                    AND property_id = 20
                    LIMIT 1) AS type_name_override,
                (SELECT id FROM entity_ids WHERE internal_id = datatype) AS type_id_override,
                EntityID AS entity_id,
                CONCAT("$", property_id) AS InternalPropertyID,
                ( SELECT id FROM entity_ids WHERE internal_id = property_id ) AS property_id
            FROM archive_data_type
            WHERE domain_id = InternalDomainID
            AND entity_id = InternalEntityID
            AND _iversion = IVersion

            UNION ALL

            -- collection
            SELECT
                collection AS collection_override,
                NULL AS name_override,
                NULL AS desc_override,
                NULL AS type_name_override,
                NULL AS type_id_override,
                EntityID AS entity_id,
                CONCAT("$", property_id) AS InternalPropertyID,
                ( SELECT id FROM entity_ids WHERE internal_id = property_id ) AS property_id
            FROM archive_collection_type
            WHERE domain_id = InternalDomainID
            AND entity_id = InternalEntityID
            AND _iversion = IVersion;

            LEAVE retrieveOverridesBody;
        END IF;
    END IF;

    SELECT
        NULL AS collection_override,
        name AS name_override,
        NULL AS desc_override,
        NULL AS type_name_override,
        NULL AS type_id_override,
        EntityID AS entity_id,
        CONCAT("$", property_id) AS InternalPropertyID,
        ( SELECT id FROM entity_ids WHERE internal_id = property_id ) AS property_id
    FROM name_overrides
    WHERE domain_id = InternalDomainID
    AND entity_id = InternalEntityID

    UNION ALL

    SELECT
        NULL AS collection_override,
        NULL AS name_override,
        description AS desc_override,
        NULL AS type_name_override,
        NULL AS type_id_override,
        EntityID AS entity_id,
        CONCAT("$", property_id) AS InternalPropertyID,
        ( SELECT id FROM entity_ids WHERE internal_id = property_id ) AS property_id
    FROM desc_overrides
    WHERE domain_id = InternalDomainID
    AND entity_id = InternalEntityID

    UNION ALL

    SELECT
        NULL AS collection_override,
        NULL AS name_override,
        NULL AS desc_override,
        (SELECT value FROM name_data
            WHERE domain_id = 0
            AND entity_ID = datatype
            AND property_id = 20 LIMIT 1) AS type_name_override,
        (SELECT id FROM entity_ids WHERE internal_id = datatype) AS type_id_override,
        EntityID AS entity_id,
        CONCAT("$", property_id) AS InternalPropertyID,
        ( SELECT id FROM entity_ids WHERE internal_id = property_id ) AS property_id
    FROM data_type
    WHERE domain_id = InternalDomainID
    AND entity_id = InternalEntityID

    UNION ALL

    SELECT
        collection AS collection_override,
        NULL AS name_override,
        NULL AS desc_override,
        NULL AS type_name_override,
        NULL AS type_id_override,
        EntityID AS entity_id,
        CONCAT("$", property_id) AS InternalPropertyID,
        ( SELECT id FROM entity_ids WHERE internal_id = property_id ) AS property_id
    FROM collection_type
    WHERE domain_id = InternalDomainID
    AND entity_id = InternalEntityID;


END;
//
delimiter ;

/*
 * This file is a part of the CaosDB Project.
 *
 * Copyright (C) 2018 Research Group Biomedical Physics,
 * Max-Planck-Institute for Dynamics and Self-Organization Göttingen
 * Copyright (C) 2020,2023 IndiScale GmbH <info@indiscale.com>
 * Copyright (C) 2020,2023 Timm Fitschen <t.fitschen@indiscale.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */

DROP PROCEDURE IF EXISTS db_5_0.updateEntity;
delimiter //

/*
 * Update an entity (that is, its metadata like name, description, ...).
 *
 * At the moment, the version ID is generated internally.
 *
 * Selects the new version identifier for the entity.
 */
CREATE PROCEDURE db_5_0.updateEntity(
    in EntityID VARCHAR(255),
    in EntityName VARCHAR(255),
    in EntityDescription TEXT,
    in EntityRole VARCHAR(255),
    in DatatypeID VARCHAR(255),
    in Collection VARCHAR(255),
    in ACL VARBINARY(65525))
BEGIN
    DECLARE ACLID INT UNSIGNED DEFAULT NULL;
    DECLARE Hash VARBINARY(255) DEFAULT NULL;
    DECLARE Version VARBINARY(255) DEFAULT SHA1(UUID());
    DECLARE ParentVersion VARBINARY(255) DEFAULT NULL;
    DECLARE Transaction VARBINARY(255) DEFAULT NULL;
    DECLARE OldIVersion INT UNSIGNED DEFAULT NULL;
    DECLARE InternalEntityID INT UNSIGNED DEFAULT NULL;

    SELECT internal_id INTO InternalEntityID from entity_ids WHERE id = EntityID;

    call entityACL(ACLID, ACL);

    IF is_feature_config("ENTITY_VERSIONING", "ENABLED") THEN
        SELECT max(_iversion) INTO OldIVersion
            FROM entity_version
            WHERE entity_id = InternalEntityID;

        -- move old data to archives
        INSERT INTO archive_entities (id, description, role,
                acl, _iversion)
            SELECT e.id, e.description, e.role, e.acl, OldIVersion
            FROM entities AS e
            WHERE e.id = InternalEntityID;

        INSERT INTO archive_data_type (domain_id, entity_id, property_id,
                datatype, _iversion)
            SELECT e.domain_id, e.entity_id, e.property_id, e.datatype,
                OldIVersion
            FROM data_type AS e
            WHERE e.domain_id = 0
            AND e.entity_id = 0
            AND e.property_id = InternalEntityID;

        INSERT INTO archive_collection_type (domain_id, entity_id, property_id,
                collection, _iversion)
            SELECT e.domain_id, e.entity_id, e.property_id, e.collection,
                OldIVersion
            FROM collection_type as e
            WHERE e.domain_id = 0
            AND e.entity_id = 0
            AND e.property_id = InternalEntityID;


        SET Transaction = @SRID;
        SELECT e.version INTO ParentVersion
            FROM entity_version as e
            WHERE e.entity_id = InternalEntityID
            AND e._iversion = OldIVersion;
        CALL insert_single_child_version(
            InternalEntityID, Hash, Version,
            ParentVersion, Transaction);
    END IF;

    UPDATE entities e
        SET e.description = EntityDescription,
            e.role=EntityRole,
            e.acl = ACLID
        WHERE e.id = InternalEntityID;

    -- clean up primary name, because updateEntity might be called without a
    -- prior call to deleteEntityProperties.
    DELETE FROM name_data
        WHERE domain_id = 0 AND entity_id = InternalEntityID AND property_id = 20;
    IF EntityName IS NOT NULL THEN
        INSERT INTO name_data
                (domain_id, entity_id, property_id, value, status, pidx)
            VALUES (0, InternalEntityID, 20, EntityName, "FIX", 0);
    END IF;

    DELETE FROM data_type
        WHERE domain_id=0 AND entity_id=0 AND property_id=InternalEntityID;

    DELETE FROM collection_type
        WHERE domain_id=0 AND entity_id=0 AND property_id=InternalEntityID;

    IF DatatypeID IS NOT NULL THEN
        INSERT INTO data_type (domain_id, entity_id, property_id, datatype)
            SELECT 0, 0, InternalEntityID,
                ( SELECT internal_id FROM entity_ids WHERE id = DatatypeID );

        IF Collection IS NOT NULL THEN
            INSERT INTO collection_type (domain_id, entity_id, property_id,
                    collection)
                SELECT 0, 0, InternalEntityID, Collection;
        END IF;
    END IF;

    Select Version as Version;

END;
//
delimiter ;

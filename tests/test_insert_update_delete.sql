/**
 * This file is a part of the CaosDB Project.
 *
 * Copyright (C) 2023 IndiScale GmbH <info@indiscale.com>
 * Copyright (C) 2023 Timm Fitschen <t.fitschen@indiscale.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */
USE _caosdb_schema_unit_tests;
BEGIN;
CALL tap.no_plan();

-- SETUP

SET @EntityID1 = 10001;
SET @EntityID2 = 10002;

-- Disable versioning and only test the non-versioning behavior
DELETE FROM feature_config WHERE _key = "ENTITY_VERSIONING";

CALL entityACL(@ACLID1, "{acl1}");
CALL entityACL(@ACLID2, "{acl2}");
SELECT entity_id into @TextDatatypeID FROM name_data WHERE value ="TEXT";

-- TESTS

-- TEST insertEntity
SELECT tap.eq(COUNT(id), 0, "No entities")
    FROM entities WHERE id>=100;
CALL insertEntity(@EntityID1, "EntityName", "EntityDesc", "RECORDTYPE", "{acl1}");

SELECT tap.eq(COUNT(entity_id), 1, "Entity has been inserted")
    FROM name_data WHERE value="EntityName";

SELECT entity_id INTO @InternalEntityID FROM name_data WHERE value="EntityName";
SELECT tap.ok(@InternalEntityID >= 100, "EntityID greater 99");

SELECT tap.eq(acl, @ACLID1, "correct acl id had been assigned")
    FROM entities WHERE id=@InternalEntityID;


-- TEST insertEntityProperty
CALL insertEntity(@EntityID2, "AProperty", "APropDesc", "PROPERTY", "{acl1}");
SELECT entity_id INTO @InternalPropertyID FROM name_data WHERE value="AProperty";
INSERT INTO data_type (domain_id, entity_id, property_id, datatype) VALUES (0, 0, @InternalPropertyID, @TextDatatypeID);

SELECT COUNT(*) INTO @x FROM null_data;
SELECT tap.eq(@x, 0, "No data in null_data table");
CALL insertEntityProperty(0, @EntityID1, @EntityID2, "null_data", NULL, NULL, "RECOMMENDED", NULL, NULL, NULL, NULL, 0);
SELECT COUNT(*) INTO @x FROM null_data;
SELECT tap.eq(@x, 1, "One row in null_data table");

-- TEST updateEntity
CALL updateEntity(@EntityID1, "NewEntityName", "NewEntityDesc", "RECORD", NULL, NULL, "{acl2}");

SELECT tap.eq(COUNT(entity_id), 0, "Old Entity name not present")
    FROM name_data WHERE value="EntityName";
SELECT tap.eq(COUNT(entity_id), 1, "Entity name has been updated")
    FROM name_data WHERE value="NewEntityName";

SELECT tap.eq(acl, @ACLID2, "acl has been updated")
    FROM entities WHERE id=@InternalEntityID;

-- CALL updateEntity again an update the Name
CALL updateEntity(@EntityID2, "NewerEntityName", "NewerEntityDesc", "RECORD", NULL, NULL, "{acl2}");
CALL updateEntity(@EntityID2, "NewEntityName", "NewEntityDesc", "RECORD", NULL, NULL, "{acl2}");


-- TEST deleteEntityProperties
CALL deleteEntityProperties(@EntityID1);
SELECT COUNT(*) INTO @x FROM null_data;
SELECT tap.eq(@x, 0, "data removed from null_data table");

-- TEST deleteEntity
CALL deleteEntity(@EntityID1);
CALL deleteEntity(@EntityID2);
SELECT COUNT(id) INTO @x FROM entities WHERE id>100;
SELECT tap.eq(@x, 0, "entity deleted");


-- TESTS END

CALL tap.finish();
ROLLBACK;


/**
 * ** header v3.0
 * This file is a part of the CaosDB Project.
 *
 * Copyright (C) 2020 IndiScale GmbH <info@indiscale.com>
 * Copyright (C) 2020 Timm Fitschen <t.fitschen@indiscale.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 *
 * ** end header
 */

/**
 * Execute an SQL statement and pass if the statement throws an SQLEXCEPTION.
 * Otherwise, fail with the given msg.
 */
DROP PROCEDURE IF EXISTS tap._assert_throws;
delimiter //
CREATE PROCEDURE tap._assert_throws(in stmt TEXT, in msg TEXT)
BEGIN
    DECLARE EXIT HANDLER FOR SQLEXCEPTION BEGIN
        SELECT tap.pass(msg);
    END;
    SET @stmt = stmt;
    PREPARE _stmt from @stmt;
    EXECUTE _stmt;

    SELECT tap.fail(msg);
END //

delimiter ;


-- test helper
-- CALL tap._assert_throws("SELECT * FROM non-existing", "should throw an error");

/**
 * This file is a part of the CaosDB Project.
 *
 * Copyright (C) 2020,2023 IndiScale GmbH <info@indiscale.com>
 * Copyright (C) 2020 Daniel Hornung <d.hornung@indiscale.com>
 * Copyright (C) 2023 Timm Fitschen <t.fitschen@indiscale.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */

USE _caosdb_schema_unit_tests;
BEGIN;
CALL tap.no_plan();

SET @EntityID1 = 10001;
SET @EntityID2 = 10002;
SET @EntityID3 = 10003;
SET @EntityID4 = 10004;

-- ########################################################################
-- TEST Issues from https://gitlab.com/caosdb/caosdb-mysqlbackend/-/issues
-- ########################################################################

-------------------------------------------------------------------------------
--                                  Issue 21                                 --
-- Deleting a child with 3 levels of ancestors.                              --
-------------------------------------------------------------------------------

-- Setup
SET @SRID = "SRID_issue_21";
INSERT INTO transactions (srid,seconds,nanos,username,realm) VALUES
(@SRID, 1234, 2345, "me", "home");
CALL entityACL(@ACLID1, "{acl1}");

-- Insert entities and obtain IDs
CALL insertEntity(@EntityID1, "A", "Desc A", "RECORDTYPE", "{acl1}");
CALL insertEntity(@EntityID2, "B", "Desc B", "RECORDTYPE", "{acl1}");
CALL insertEntity(@EntityID3, "C", "Desc C", "RECORDTYPE", "{acl1}");
CALL insertEntity(@EntityID4, "rec", "Desc rec", "RECORD", "{acl1}");

-- Insert is-a relationships
CALL insertIsA(@EntityID1, @EntityID2);
CALL insertIsA(@EntityID2, @EntityID3);
CALL insertIsA(@EntityID4, @EntityID1);

-- Try to delete last child
-- leads to failure in issue #21
CALL deleteIsa(@EntityID4);

ROLLBACK;

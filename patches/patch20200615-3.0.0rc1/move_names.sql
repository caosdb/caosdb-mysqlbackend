/*
 * This file is a part of the CaosDB Project.
 *
 * Copyright (C) 2020 IndiScale GmbH <info@indiscale.com>
 * Copyright (C) 2020 Timm Fitschen <t.fitschen@indiscale.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */

/*
 * This patch moves all names of existing entities from the entities table into
 * the name_data table.
 */


-- unique key adds savety in writing transactions and performance in reading
-- transactions.
ALTER TABLE name_data ADD UNIQUE KEY (domain_id, entity_id, property_id);

-- copy names of old entities. 20 is the (hard-coded) id of the name property
-- itself. status is "FIX" because names are not to be inherited (by default).
-- pidx 0 is irrelevant here because the primary names have no "order" in which
-- they appear.
INSERT INTO name_data (domain_id, entity_id, property_id, value, status, pidx) SELECT 0, id, 20, name, "FIX", 0 FROM entities WHERE name IS NOT NULL;

-- delete the name column and effectively all the names from entities table.
ALTER TABLE entities DROP COLUMN name;

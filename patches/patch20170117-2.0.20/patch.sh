#!/bin/bash
#
# ** header v3.0
# This file is a part of the CaosDB Project.
#
# Copyright (C) 2018 Research Group Biomedical Physics,
# Max-Planck-Institute for Dynamics and Self-Organization Göttingen
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.
#
# ** end header
#
# add NOT NULL to data_type table
# Update mysql schema to version v2.0.20
NEW_VERSION="v2.0.20"
OLD_VERSION="v2.0.19"

if [ -z "$UTILSPATH" ]; then
 UTILSPATH="../utils"
fi

. $UTILSPATH/patch_header.sh $*


check_version $OLD_VERSION

mysql_execute 'SET FOREIGN_KEY_CHECKS=0; 
                       ALTER TABLE data_type MODIFY COLUMN domain_id INT UNSIGNED NOT NULL;
                       ALTER TABLE data_type MODIFY COLUMN entity_id INT UNSIGNED NOT NULL;
                       ALTER TABLE data_type MODIFY COLUMN property_id INT UNSIGNED NOT NULL;
                       ALTER TABLE data_type MODIFY COLUMN datatype INT UNSIGNED NOT NULL;
					   SET FOREIGN_KEY_CHECKS=1;'


update_version $NEW_VERSION

success

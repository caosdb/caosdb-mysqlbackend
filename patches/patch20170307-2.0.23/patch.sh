#!/bin/bash
#
# ** header v3.0
# This file is a part of the CaosDB Project.
#
# Copyright (C) 2018 Research Group Biomedical Physics,
# Max-Planck-Institute for Dynamics and Self-Organization Göttingen
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.
#
# ** end header
#
# Updates access control tables
# Update mysql schema to version v2.0.23
NEW_VERSION="v2.0.23"
OLD_VERSION="v2.0.22"

if [ -z "$UTILSPATH" ]; then
 UTILSPATH="../utils"
fi

. $UTILSPATH/patch_header.sh $*


check_version $OLD_VERSION

mysql_execute 'DELETE FROM permissions;'
mysql_execute 'DELETE FROM roles;'
mysql_execute 'DELETE FROM passwd;'

mysql_execute 'INSERT INTO roles (name, description) VALUES ("administration","Users with this role have unrestricted permissions.");'

mysql_execute 'INSERT INTO permissions (role, permissions) VALUES ("administration","[{\"grant\":\"true\",\"priority\":\"true\",\"permission\":\"*\"}]");'

mysql_execute 'DROP TABLE subjects;'
mysql_execute 'CREATE TABLE `user_info` (
  `realm` varbinary(255) NOT NULL,
  `name` varbinary(255) NOT NULL,
  `email` varbinary(255) DEFAULT NULL,
  `status` enum("ACTIVE","INACTIVE") COLLATE utf8_unicode_ci NOT NULL DEFAULT "INACTIVE",
  `entity` int(10) unsigned DEFAULT NULL,
  PRIMARY KEY (`realm`,`name`),
  KEY `subject_entity` (`entity`),
  CONSTRAINT `subjects_ibfk_1` FOREIGN KEY (`entity`) REFERENCES `entities` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;'

mysql_execute 'DROP TABLE membership;' 
mysql_execute 'CREATE TABLE `user_roles` (
  `realm` varbinary(255) NOT NULL,
  `user` varbinary(255) NOT NULL,
  `role` varbinary(255) NOT NULL,
  PRIMARY KEY (`realm`,`user`,`role`),
  CONSTRAINT `user_roles_ibfk_1` FOREIGN KEY (`role`) REFERENCES `roles` (`name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci'

update_version $NEW_VERSION

success

/*
 * This file is a part of the CaosDB Project.
 *
 * Copyright (C) 2022-2023 IndiScale GmbH <info@indiscale.com>
 * Copyright (C) 2022-2023 Timm Fitschen <t.fitschen@indiscale.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */

-- a little bit of house keeping
DROP PROCEDURE IF EXISTS retrieveSubEntity;
DROP PROCEDURE IF EXISTS retrieveDatatype;
DROP PROCEDURE IF EXISTS retrieveGroup;
DROP PROCEDURE IF EXISTS getInfo;
DROP PROCEDURE IF EXISTS getRole;
DROP PROCEDURE IF EXISTS setPassword;
DROP PROCEDURE IF EXISTS initAutoIncrement;
DELETE FROM name_data WHERE entity_id=50;
DELETE FROM entities WHERE id=50;
DELETE FROM entities WHERE id=99;

-- this simply is the more appropriate name
ALTER TABLE entities MODIFY COLUMN
  `role` enum('RECORDTYPE','RECORD','FILE','DOMAIN','PROPERTY','DATATYPE','ROLE','QUERYTEMPLATE', '_REPLACEMENT') COLLATE utf8_unicode_ci NOT NULL;
ALTER TABLE archive_entities MODIFY COLUMN
  `role` enum('RECORDTYPE','RECORD','FILE','DOMAIN','PROPERTY','DATATYPE','ROLE','QUERYTEMPLATE', '_REPLACEMENT') COLLATE utf8_unicode_ci NOT NULL;
UPDATE entities SET role = "_REPLACEMENT" WHERE role="DOMAIN";
UPDATE archive_entities SET role = "_REPLACEMENT" WHERE role="DOMAIN";
ALTER TABLE entities MODIFY COLUMN
  `role` enum('RECORDTYPE','RECORD','FILE','_REPLACEMENT','PROPERTY','DATATYPE','ROLE','QUERYTEMPLATE') COLLATE utf8_unicode_ci NOT NULL;
ALTER TABLE archive_entities MODIFY COLUMN
  `role` enum('RECORDTYPE','RECORD','FILE','_REPLACEMENT','PROPERTY','DATATYPE','ROLE','QUERYTEMPLATE') COLLATE utf8_unicode_ci NOT NULL;



-- new entity_ids table
DROP TABLE IF EXISTS `entity_ids`;
CREATE TABLE `entity_ids` (
    `id` VARCHAR(255) NOT NULL,
    `internal_id` int(10) unsigned NOT NULL COMMENT 'Internal ID of an entity. This id is used internally in the *_data tables and elsewhere. This ID is never exposed via the CaosDB API.',
    PRIMARY KEY `entity_ids_pk` (`id`),
    CONSTRAINT `entity_ids_internal_id` FOREIGN KEY (`internal_id`) REFERENCES `entities` (`id`)
) ENGINE=InnoDB COLLATE utf8mb4_bin;

-- fill all existing entities into the new entity_ids table.
INSERT INTO entity_ids (id, internal_id) SELECT id, id FROM entities WHERE id>0 AND role!="_REPLACEMENT";

ALTER TABLE transaction_log MODIFY COLUMN `entity_id` VARCHAR(255) COLLATE utf8mb4_bin NOT NULL;

ALTER TABLE user_info DROP FOREIGN KEY `subjects_ibfk_1`;
ALTER TABle user_info MODIFY COLUMN `entity` VARCHAR(255) COLLATE utf8mb4_bin DEFAULT NULL;
ALTER TABLE user_info ADD CONSTRAINT `subjects_ibfk_2` FOREIGN KEY (`entity`) REFERENCES `entity_ids` (`id`);

#!/bin/bash
#
# ** header v3.0
# This file is a part of the CaosDB Project.
#
# Copyright (C) 2018 Research Group Biomedical Physics,
# Max-Planck-Institute for Dynamics and Self-Organization Göttingen
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.
#
# ** end header
#
# remove refid and exponent
# Update mysql schema to version v2.0.21
NEW_VERSION="v2.0.21"
OLD_VERSION="v2.0.20"

if [ -z "$UTILSPATH" ]; then
 UTILSPATH="../utils"
fi

. $UTILSPATH/patch_header.sh $*


check_version $OLD_VERSION

function delete_exponent { 
		mysql_execute 'SET autocommit=0; DELETE FROM data_type WHERE domain_id=0 AND entity_id=0 AND property_id=22; DELETE FROM entities WHERE id = 22; COMMIT;'
		
	
}

function transform_refid_to_references {
		mysql_execute 'DELETE FROM data_type WHERE data_type.datatype=11 AND EXISTS (SELECT 0 FROM reference_data AS r WHERE r.domain_id=data_type.entity_id AND r.entity_id=data_type.property_id AND r.property_id=25);'
		mysql_execute 'INSERT INTO data_type SELECT 0 as domain_id, r1.domain_id AS entity_id, r1.entity_id AS property_id, r1.value as datatype FROM reference_data AS r1 WHERE r1.property_id=25 AND (domain_id=0 OR NOT EXISTS (SELECT * FROM reference_data AS r2 WHERE r2.domain_id=0 and r2.entity_id=r1.entity_id AND r2.value=r1.value));'
    mysql_execute 'DELETE FROM reference_data WHERE property_id = 25; DELETE FROM data_type WHERE property_id=25;'

}

function delete_refid {
    mysql_execute 'DELETE FROM entities WHERE id = 25;'
}

delete_exponent
transform_refid_to_references
delete_refid

update_version $NEW_VERSION

success

# This file is a part of the Linkahead Project.
#
# Copyright (C) 2018 Research Group Biomedical Physics,
#   Max-Planck-Institute for Dynamics and Self-Organization Göttingen
# Copyright (C) 2019, 2020, 2024 Daniel Hornung <d.hornung@indiscale.com>
# Copyright (C) 2020 Henrik tom Wörden
# Copyright (C) 2020, 2024 IndiScale GmbH <info@indiscale.com>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.

# Load setting from different files, in this order:
# - config.defaults (in the current directory)
# - .config (in the current directory)
# - The file given in ENV_FILE.

if [ -z "$MAINPATH" ]; then
    if [ -z "$UTILSPATH" ] ; then
        echo "At least one of MAINPATH and UTILSPATH must be defined in load_settings.sh" >&2
        exit 1
    fi
    MAINPATH="$(dirname $UTILSPATH)"
    export MAINPATH
fi

# Make a backup of the important environment variables.
# Take care: Assignments may be more than one line!
_expr="(MYSQL|DATABASE)"
my_env=$(export -p | awk '/^declare -x '"$_expr"'/                   {PRINT=1} \
                          /^declare -x/ && !/^declare -x '"$_expr"'/ {PRINT=0} \
                          PRINT                                      {print}' \
             || true)

source "$MAINPATH/config.defaults"

if [[ -r "$MAINPATH/.config" ]] ; then
    source "$MAINPATH/.config"
fi
if [[ -n "$ENV_FILE" ]] ; then
    source "$ENV_FILE"
fi

if [[ -z "$MYSQL_CMD" ]] ; then
    echo "'mysql' executable not defined!"
    exit 1
fi

# Restore backup.
eval "$my_env"

export MYSQL_CMD
export MYSQLDUMP_CMD
export MYSQLADMIN_CMD
export MYSQL_CONFIG_EDITOR_CMD

export MYSQL_HOST
export MYSQL_PORT
export MYSQL_USER
export MYSQL_OPTS

export DATABASE_NAME
export DATABASE_USER
export DATABASE_USER_PW
export DATABASE_USER_HOST_LIST

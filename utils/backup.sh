#!/bin/bash
#
# ** header v3.0
# This file is a part of the CaosDB Project.
#
# Copyright (C) 2018 Research Group Biomedical Physics,
# Max-Planck-Institute for Dynamics and Self-Organization Göttingen
# Copyright (C) 2020 Daniel Hornung <d.hornung@indiscale.com>
# Copyright (C) 2020 IndiScale <info@indiscale.com>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.
#
# ** end header
#

# Dump a database with all procedures, permissions, structure and data

if [ -z "$UTILSPATH" ]; then
    UTILSPATH="$(realpath $(dirname $0))"
    export UTILSPATH
fi

# The directory which the dump is to be stored to. Do not change it here. Use
# the environment variable instead.
BACKUPDIR="${BACKUPDIR:-../backup}"

# Load settings from .config and defaults #####################################
. $UTILSPATH/load_settings.sh

# load useful functions #######################################################
. $UTILSPATH/helpers.sh

function backup() {
    database="$1"
    backupdir="$2"

    # Assert backup dir
    mkdir -p "$backupdir"

    datestring=$(date -u --rfc-3339=ns | sed 's/ /T/g')
    backupfile=${BACKUPDIR}/${DATABASE_NAME}.${datestring}.dump.sql

    if [ -e "$backupfile" ]; then
        failure "Dumpfile '$backupfile' already exists."
    fi

    echo "Dumping database $database to $backupfile ..."
    $MYSQLDUMP_CMD $(get_db_args_nodb) --opt --default-character-set=utf8 \
          --routines "$database" > "$backupfile"

    success
}

backup $DATABASE_NAME $BACKUPDIR

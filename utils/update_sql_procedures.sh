#!/bin/bash
#
# This file is a part of the LinkAhead Project.
#
# Copyright (C) 2018 Research Group Biomedical Physics,
#   Max-Planck-Institute for Dynamics and Self-Organization Göttingen
# Copyright (C) 2020, 2024 Daniel Hornung <d.hornung@indiscale.com>
# Copyright (C) 2020 Henrik tom Wörden <h.tomwoerden@indiscale.com>
# Copyright (C) 2020, 2024 IndiScale <info@indiscale.com>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.
#

# Updates all SQL procedures

set -e

if [ -z "$UTILSPATH" ]; then
    UTILSPATH=$(realpath "$(dirname $0)")
    export UTILSPATH
fi

source $UTILSPATH/load_settings.sh
source $UTILSPATH/helpers.sh

echo "Updating procedures ... "
temp_proc_sql=$(mktemp --suffix=.sql)
sed -e "s/db_5_0/$DATABASE_NAME/g" procedures/*.sql procedures/query/*.sql \
    > "$temp_proc_sql"
mysql_execute_file "$temp_proc_sql"
# We keep this for debugging purposes...
# rm "$temp_proc_sql"

success


